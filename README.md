It will deploy GKE cluster with 1 node pool of n1-standard-2 instance type.

Terraform will store state in GCS bucket.

Gitlab-CI contains following jobs:

- validate: run `terraform validate`
- plan: build plan of planned changes: `terraform plan`
- apply: apply changes (and build plan again): `terraform apply`
- verify: build plan and check that everything is fine (could be useful to compare before/after)
- destroy: destroy everything: `terraform destroy`

Required variables:

| Name | Possible value | Description |
| ------ | ------ | ------ |
| GOOGLE_CREDENTIALS | json | JSON-file with authentification info to be able deploy using terraform SA ([more](https://cloud.google.com/iam/docs/creating-managing-service-account-keys#creating_service_account_keys)) |
| ISTIO_DISABLED | default: true | If false, istio add-on will be installed |
| PROJECT_ID | "infra-265919" | unique project id|
|REGION | "us-central1" | region id |
|ZONE| "us-central1-a" | zone id |

---
NOTES:
- validate and plan will be run at every time
- apply, verify, destroy only from **master** branch