resource "google_service_account" "gitlab" {
  account_id   = "gitlab"
  display_name = "Gitlab service account"
  description  = "Account for deploying GKE from gitlab"
}

resource "google_project_iam_member" "gitlab-iam" {
  project = var.project_id
  role    = "roles/container.admin"
  member  = "serviceAccount:${google_service_account.gitlab.email}"
}