resource google_container_cluster "cluster" {
  provider                 = google-beta
  name                     = var.name
  location                 = var.zone
  remove_default_node_pool = true
  initial_node_count       = 1
  monitoring_service       = "none"
  logging_service          = "none"
  min_master_version       = "1.15"

  network_policy {
    enabled = true
    provider = "CALICO"
  }
  // temporarily disable istio
  addons_config {
    istio_config {
      disabled = var.istio_disabled
    }
  }
  maintenance_policy {
    daily_maintenance_window {
      start_time = "02:00"
    }
  }
}

resource google_container_node_pool "node_pool" {
  name               = var.name
  location           = var.zone
  cluster            = google_container_cluster.cluster.name
  version            = "1.15.11-gke.1"
  initial_node_count = 1

  autoscaling {
    min_node_count = 1
    max_node_count = 5
  }

  node_config {
    preemptible  = true
    machine_type = var.worker_machine_type
    disk_size_gb = var.worker_disk_size

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/monitoring.write",
    ]
  }

  timeouts {
    create = "60m"
    update = "60m"
    delete = "60m"
  }
}

resource google_compute_address "address" {
  name         = var.name
  address_type = "EXTERNAL"
}
