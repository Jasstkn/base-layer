terraform {
  backend "gcs" {
    bucket = "tf-state-lecture-demo"
    prefix = "terraform/state"
  }
}